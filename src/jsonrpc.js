(function() {
  'use strict';

  angular.module('angular-jsonrpc', [])
         .service('jsonrpc', jsonrpc);

  jsonrpc.$inject = ['$q', '$http'];

  var config = {
    protocol: 'http',
    hostname: 'localhost',
    port    : 8000,
    path    : '/'
  };

  var id = 0;

  function jsonrpc($q, $http) {
    return {
      request: request,
      configure: configure,
      getConfig: getConfig
    };

    function _getFullUrl() {
      return config.protocol + '://' + config.hostname + ':' + config.port + config.path;
    }

    function _getInputData(methodName, args) {
      id += 1;
      return {
        jsonrpc: '2.0',
        id     : id,
        method : methodName,
        params : args
      }
    }

    function configure(args) {
      if (typeof(args) !== 'object') {
        throw new Error('Argument of "configure" must be an object.');
      }
      var allowedKeys = ['protocol', 'hostname', 'port', 'path'];
      var keys = Object.keys(args);
      keys.forEach(function(key) {
        if (allowedKeys.indexOf(key) < 0) {
          throw new Error('Invalid configuration key "' + key + "'. Allowed keys are: " +
            allowedKeys.join(', '));
        }
      });

      keys.forEach(function(key) {
        config[key] = args[key];
      });
    }

    function getConfig() {
      // We create a copy of the config object, to make sure that the return value
      // of this call doesn't "magically" change when calling jsonrpc.configure()
      // later on.
      var currentConfig = {};
      Object.keys(config).forEach(function(key) {
        currentConfig[key] = config[key];
      })
      return currentConfig;
    }

    function request(methodName, args) {
      var deferred = $q.defer();

      var url = _getFullUrl();
      var inputData = _getInputData(methodName, args);

      var req = {
       method: 'POST',
       url: url,
       headers: {
         'Content-Type': 'application/json'
       },
       data: inputData
      };

      $http(req)
        .success(function(response) {
          deferred.resolve(response);
        })
        .error(function(err) {
          deferred.reject(err);
        });

      return deferred.promise;
    }    
  }

}());
