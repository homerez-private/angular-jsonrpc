'use strict';
describe('jsonrpc module', function() {
  beforeEach(module('angular-jsonrpc'));

  describe('jsonrpc.getConfig', function() {
    it('should get the default configuration', function(done) {
      inject(function(jsonrpc) {
        // given default config
      
        // when
        var config = jsonrpc.getConfig();

        // then
        var expectedConfig = {
          protocol: 'http',
          hostname: 'localhost',
          port    : 8000,
          path    : '/'
        };
        verifyConfig(config, expectedConfig);
        done();
      });
    });
  });

  describe('jsonrpc.configure', function() {
    var defaultExpectedConfig = {
      protocol: 'http',
      hostname: 'localhost',
      port    : 8000,
      path    : '/'
    };

    var testCases = [
      {
        name: 'set protocol to https',
        args: {
          protocol: 'https'
        },
        expectedConfig: {
          protocol: 'https'
        }
      },
      {
        name: 'all four config parameters',
        args: {
          protocol: 'https',
          hostname: 'example.com',
          port    : 1729,
          path    : '/42',
        },
        expectedConfig: {
          protocol: 'https',
          hostname: 'example.com',
          port    : 1729,
          path    : '/42',
        }
      },
    ];

    testCases.forEach(function(tc) {
      it('should set the configuration: ' + tc.name, function(done) {
        inject(function(jsonrpc) {
          // We need to restore the configuration at the end of this test.
          var oldConfig = jsonrpc.getConfig();

          // given
          jsonrpc.configure(tc.args);

          // when
          var config = jsonrpc.getConfig();

          // then
          var expectedConfig = {};
          Object.keys(defaultExpectedConfig).forEach(function(key) {
            expectedConfig[key] = defaultExpectedConfig[key];
          });
          Object.keys(tc.expectedConfig).forEach(function(key) {
            expectedConfig[key] = tc.expectedConfig[key];
          });

          verifyConfig(config, expectedConfig);

          // Reset the configuration to the original configuration.
          jsonrpc.configure(oldConfig);
          done();
        });
      });
    })
  });

  describe('jsonrpc.request', function() {
    it('should perform a jsonrpc call to the default backend when unconfigured', function(done) {
      inject(function(jsonrpc, $injector, $http) {
        var methodName = 'version';
        var args = { };

        var $httpBackend = $injector.get('$httpBackend');

        var expected = {
          method: 'POST',
          url: 'http://localhost:8000/',
          body: {
            jsonrpc: "2.0",
            method: methodName,
            params: {},
            id: 1
          }
        }

        var returnValue = {
          jsonrpc: "2.0",
          id: 1,
          result: { version: '1.7.9' }
        }
        var authRequestHandler = $httpBackend.when(expected.method, expected.url, expected.body)
                                 .respond(returnValue);

        jsonrpc.request(methodName, args)
          .then(function(data) {
            expect(data.result.version).to.equal('1.7.9');
            done();
          })
          .catch(function(err) {
            // should not come here
            console.log(err);
            done(err);
          });

        $httpBackend.flush();
      });
    });
  });
});

function verifyConfig(config, expectedConfig) {
  expect(typeof(config)).to.equal('object');
  expect(Object.keys(config).length).to.equal(Object.keys(expectedConfig).length);
  Object.keys(config).forEach(function(key) {
    expect(config[key]).to.equal(expectedConfig[key]);
  });
}